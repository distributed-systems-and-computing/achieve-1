# Achievement #1 - “Параллельный” таймер AVR Assembler

1. Стандартная сложность - разработать программу на языке AVR Assembler под микроконтроллер Atmega8 (Arduino). Запускаются два параллельных таймера, параметры интервалов задаются константами TIMER1_INTERVAL и TIMER2_INTERVAL. При срабатывании первого таймера (через интервал TIMER1_INTERVAL) в USART выводится строка TIMER1_STR равная “ping\r\n”, при срабатывании второго строка (через интервал TIMER2_INTERVAL) TIMER2_STR равная “pong\r\n”.

2. Средняя сложность - выполнить задание стандартной сложности. Минимальная точность времени вывода 4 такта. Найти минимальную сумму значений TIMER1_INTERVAL и TIMER2_INTERVAL при которой программа выполняется корректно.

3. Высокая сложность - выполнить задание средней сложности. Разработать систему команд работающую через USART:
- изменение значения TIMER1_INTERVAL
- изменение значения TIMER2_INTERVAL
- перезапуск таймеров
- изменить строку TIMER1_STR
- изменить строку  TIMER2_STR


При изменении значений параметров вычислять невозможность корректного исполнения программы и выводить ошибку. На время вычислений таймеры останавливать нельзя.

### Полезные ссылки.

Видео урок по работе с таймерами. [Ссылка](https://www.youtube.com/watch?v=PHDKorunI38&list=PL0IfUQKPGZJTMiIcEBo1TVI0d5BOtF_HU&index=4)

Видео урок по работе с USART. [Ссылка](https://www.youtube.com/watch?v=pG0HKHGTeIw&list=PL0IfUQKPGZJTMiIcEBo1TVI0d5BOtF_HU&index=10)

Сайт со всеми командами assembler. [Ссылка](https://dfe.karelia.ru/koi/posob/avrlab/avrasm-rus.htm)

Atmega 168 datasheet. [File.pfd](http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-9365-Automotive-Microcontrollers-ATmega88-ATmega168_Datasheet.pdf)